#!/bin/bash

###################
apt-get -y update && \
apt-get -y install \
  wget \
  unzip \
  nginx \
  php \
  php-gd \
  php-mbstring \
  php-zip \
  php-curl \
  php-fpm \
  php-xml && \
mkdir -p /var/run/php && \
chown www-data. /var/run/php && \
cd /var/run/php && \
ln -s php7.4-fpm.sock php-fpm.sock && \
###################
cd /srv && \
###################
wget https://github.com/getgrav/grav/releases/download/{{ grav_version }}/grav-admin-v{{ grav_version }}.zip && \
unzip grav-admin-v{{ grav_version }}.zip && \
mv -v grav-admin grav && \
rm *.zip && \
mv -v /var/tmp/custom.css /srv/grav/user/themes/quark/css/custom.css && \
chown -R www-data. * && \
###################
mv -v /var/tmp/php.ini /etc/php/7.4/fpm/php.ini && \
mv -v /var/tmp/nginx.conf /etc/nginx/sites-available/grav.conf && \
cd /etc/nginx/sites-enabled && \
rm default && \
ln -s ../sites-available/grav.conf
